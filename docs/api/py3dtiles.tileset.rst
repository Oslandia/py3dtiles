py3dtiles.tileset package
=========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   py3dtiles.tileset.content
   py3dtiles.tileset.extension

py3dtiles.tileset.bounding\_volume module
-----------------------------------------

.. automodule:: py3dtiles.tileset.bounding_volume
   :members:
   :show-inheritance:

py3dtiles.tileset.bounding\_volume\_box module
----------------------------------------------

.. automodule:: py3dtiles.tileset.bounding_volume_box
   :members:
   :show-inheritance:

py3dtiles.tileset.root\_property module
---------------------------------------

.. automodule:: py3dtiles.tileset.root_property
   :members:
   :show-inheritance:

py3dtiles.tileset.tile module
-----------------------------

.. automodule:: py3dtiles.tileset.tile
   :members:
   :show-inheritance:

py3dtiles.tileset.tileset module
--------------------------------

.. automodule:: py3dtiles.tileset.tileset
   :members:
   :show-inheritance:

py3dtiles.tileset.utils module
------------------------------

.. automodule:: py3dtiles.tileset.utils
   :members:
   :show-inheritance:

Module contents
---------------

.. automodule:: py3dtiles.tileset
   :members:
   :show-inheritance:
